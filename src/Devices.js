import React, { useState, useEffect, useContext, useRef } from 'react'
import { Card, Button, Table, Row, Modal, Form } from "react-bootstrap"
import Axios from 'axios'
import { Context } from './Context'

const Devices = () => {
    const [show, setShow] = useState(false)
    const [devices, setDevices] = useState([])
    const [room, setRoom] = useState(0)
    const [load, setLoad] = useState(false)
    const [device, setDevice] = useState(null)
    const { rooms, users } = useContext(Context)
    const [, forceUpdate] = React.useState(0);
    const ws = useRef(null);

    useEffect(() => {
        Refresh()
        ws.current = new WebSocket(process.env.REACT_APP_REST_API_WS + "/webws");
        ws.current.onopen = () => console.log("Connecting");
        ws.current.onclose = () => alert("WebSocket Closed. Please Refresh Page");

        return () => {
            ws.current.close();
        };
    }, [])

    useEffect(() => {
        if (!ws.current) return;
        ws.current.onmessage = e => {
            const m = JSON.parse(JSON.parse(e.data).m);
            devices.forEach((d, k) => {
                if (d.projectname !== m.projectname) {
                    return
                }
                if (d.cniaddress !== Number(m.cniaddress)) {
                    return
                }
                if (d.deviceaddress !== Number(m.deviceaddress)) {
                    return
                }
                devices[k].status = m.status
                setDevices(devices)
                console.log(devices)
                forceUpdate(n => !n)
                console.log(m)
            })
        };
    }, [load]);

    const Refresh = () => {
        Axios.get(process.env.REACT_APP_REST_API_URL + "/device", { withCredentials: true }).then((res) => {
            setDevices(res.data)
            setLoad(true)
            console.log(res.data)
        })
    }

    const Discovery = (device) => {
        Axios.post(process.env.REACT_APP_REST_API_URL + "/discovery", { device: device.device, state: !device.discovery }, { withCredentials: true }).then(() => Refresh())
    }

    const Status = (device) => {
        if (device.categories === "LIGHT") {
            Axios.post(process.env.REACT_APP_REST_API_URL + "/light", { device: device.endpointid, state: device.status === "ON" ? "OFF" : "ON" }, {
                headers: { Authorization: `Bearer ${users[0].token}` }
            }).then(() => setTimeout(() => Refresh(), 500))
        }
    }

    return (
        <div style={{ padding: 5 }}>
            <AddDevice show={show} setShow={setShow} device={device} refresh={Refresh} />
            <Card style={{ maxWidth: 1000, margin: "auto" }}>
                <Card.Body>
                    <Row>
                        <Card.Title style={{ padding: 15 }}>Devices</Card.Title>
                        <Form.Control style={{ maxWidth: 200, margin: "auto" }} as="select" value={room} onChange={e => { setRoom(Number(e.target.value))}}>
                            <option value={0}>All Devices</option>
                            {(rooms !== null)?rooms.map((r) => {
                                return <option key={r.id} value={r.id}>{r.name}</option>
                            }):null}
                        </Form.Control>
                        <Button onClick={Refresh} variant="success" style={{ margin: "auto", marginRight: 5 }}>Refresh</Button>
                        <Button onClick={() => { setShow(true); setDevice(null) }} variant="secondary" style={{ margin: "auto", marginLeft: 5, marginRight: 15 }}>Add Device</Button>
                    </Row>
                    <div style={{ overflowX: "scroll" }}>
                        <Table striped bordered hover size="sm" style={{ minWidth: 800 }}>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>RoomID</th>
                                    <th>EndpointID</th>
                                    <th>FriendlyName</th>
                                    <th>Description</th>
                                    <th>Categories</th>
                                    <th>Address</th>
                                    <th>Discovery</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    devices === null ? null : devices.map((device, key) => {
                                        if (room !== 0 && room != device.roomid){
                                            return
                                        }
                                        return (
                                            <tr >
                                                <td>{device.device}</td>
                                                <td>{device.roomid === 0 || rooms === null ? "undefined" : rooms[rooms.map(d => { return d.id }).indexOf(device.roomid)].name}</td>
                                                <td>{device.endpointid}</td>
                                                <td>{device.friendlyname}</td>
                                                <td>{device.description}</td>
                                                <td>{device.categories}</td>
                                                <td>{"//" + device.projectname + "/" + device.cniaddress + "/56/" + device.deviceaddress}</td>
                                                <td style={{ background: device.discovery ? "#00ff00" : "#FF0000", cursor: "pointer" }} onClick={(e) => { e.stopPropagation(); Discovery(device) }}> {device.discovery ? "true" : "false"}</td>
                                                <td style={{ background: device.status === "ON" ? "#00ff00" : "#FF0000", cursor: "pointer" }} onClick={(e) => { e.stopPropagation(); Status(device) }}>{device.status}</td>
                                                <td onClick={() => { setDevice(device); setShow(true) }} style={{ fontWeight: "bold", cursor: "pointer" }}><u>Edit</u></td>
                                            </tr>
                                        )

                                    })
                                }

                            </tbody>
                        </Table>
                    </div>
                </Card.Body>
            </Card >
        </div >
    )
}

const AddDevice = ({ show, setShow, device, refresh }) => {

    const { rooms } = useContext(Context)

    const [friendlyname, setFriendlyName] = useState("")
    const [description, setDescription] = useState("")
    const [categories, setCategories] = useState("")
    const [projectname, setProjectName] = useState("")
    const [cniaddress, setCNIAddress] = useState("")
    const [deviceaddress, setDeviceAddress] = useState("")
    const [roomid, setRoomID] = useState("")

    if (device !== null) {
        setFriendlyName(device.friendlyname)
        setDescription(device.description)
        setProjectName(device.projectname)
        setCNIAddress(device.cniaddress)
        setDeviceAddress(device.deviceaddress)
        setRoomID(device.roomid)
    }

    const handleClose = () => setShow(false)

    const AddDevice = () => {
        Axios.put(process.env.REACT_APP_REST_API_URL + "/device", {
            friendlyname: friendlyname,
            description: description,
            projectname: projectname,
            categories: categories,
            cniaddress: Number(cniaddress),
            deviceaddress: Number(deviceaddress),
            roomid: Number(roomid)
        }, { withCredentials: true }).then(refresh())
    }

    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Login</Modal.Title>
            </Modal.Header>
            <Form onSubmit={AddDevice}>
                <Modal.Body>

                    <Form.Group>
                        <Form.Label>Friendly Name</Form.Label>
                        <Form.Control type="text" placeholder="Friendly Name" onChange={e => setFriendlyName(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Description" onChange={e => setDescription(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Type</Form.Label>
                        <Form.Control onChange={e => setCategories(e.target.value)} as="select">
                            <option value={"LIGHT"}>Light</option>
                            <option value={"INTERIOR_BLIND"}>Curtain</option>

                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Room</Form.Label>
                        <Form.Control onChange={e => setRoomID(e.target.value)} as="select">
                            <option value={0}>undefined</option>
                            {
                                rooms === null ? null : rooms.map((room, key) => {
                                    return <option value={room.id} key={key}>{room.name}</option>
                                })
                            }

                        </Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Project Name</Form.Label>
                        <Form.Control type="text" placeholder="Project Name" onChange={e => setProjectName(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>CNI Address</Form.Label>
                        <Form.Control type="number" placeholder="CNI Address" onChange={e => setCNIAddress(e.target.value)} required />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Device Address</Form.Label>
                        <Form.Control type="number" placeholder="Device Address" onChange={e => setDeviceAddress(e.target.value)} required />
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" type="submit">Submit</Button>
                </Modal.Footer>
            </Form>
        </Modal>)
}

export default Devices