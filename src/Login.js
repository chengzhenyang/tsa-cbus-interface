import React, { useState, useContext } from "react"
import { Button, Modal, Form } from "react-bootstrap"
import Axios from "axios"
import { Context } from "./Context"

const Login = () => {
    const { login } = useContext(Context)
    const [show, setShow] = useState(false)
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const Login = () => {
        Axios.post(process.env.REACT_APP_REST_API_URL + "/login", {
            username: username, password: password
        }, { withCredentials: true }).then(res => {
            setShow(false)
            window.location.reload();
        }).catch(err => {
            alert("Login Wrong Password")
        })
    }

    const Logout = () => {
        Axios.get(process.env.REACT_APP_REST_API_URL + "/logout", { withCredentials: true })
        window.location.reload();
    }

    return (
        <>
            {login ? <Button variant="secondary" onClick={Logout}>Logout</Button> : <Button variant="secondary" onClick={handleShow}>Login</Button>}

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Username" onChange={e => setUsername(e.target.value)} required />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required />
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" type="submit" onClick={Login}>Submit</Button>
                </Modal.Footer>
            </Modal>
        </>)
}

export default Login