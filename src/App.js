import React from 'react'
import {HashRouter, Route} from 'react-router-dom'
import './App.css'
import TopBar from './TopBar'
import Users from './Users'
import Devices from './Devices'
import Discovery from './Discovery'
import Provider from './Context'
import Console from './Console'
import Home from './Home'

function App() {
  return (
    <Provider>
      <TopBar />
      <HashRouter basename="/tsa">
        <Route exact path="/" component={Home} />
        <Route exact path="/Users" component={Users} />
        <Route exact path="/Devices" component={Devices} />
        <Route exact path="/Discovery" component={Discovery} />
        <Route exact path="/Console" component={Console} />
      </HashRouter>

    </Provider>
  );
}

export default App;
