import React, { useState, useEffect, useContext } from 'react'
import { Card, Button, Table, Row } from "react-bootstrap"
import { Context } from './Context'

const Home = () => {
    const { rooms } = useContext(Context)
    return (
        <div style={{ padding: 5 }}>
            <Card style={{ maxWidth: 1000, margin: "auto" }}>
                <Card.Body>
                    <Row>
                        <Card.Title style={{ padding: 15 }}>Rooms</Card.Title>
                        <Button variant="secondary" style={{ margin: "auto", marginRight: 15 }}>Add User</Button>
                    </Row>
                    <div style={{ overflowX: "scroll" }}>
                        <Table striped bordered hover size="sm" style={{ minWidth: 800 }}>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    rooms === null ? null : rooms.map((room, key) => {
                                        return (
                                            <tr>
                                                <td>{room.id}</td>
                                                <td>{room.name}</td>
                                            </tr>
                                        )

                                    })
                                }

                            </tbody>
                        </Table>
                    </div>
                </Card.Body>
            </Card >
        </div >
    )
}

export default Home