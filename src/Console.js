import React, { useState, useEffect, useRef } from 'react'

const Console = () => {
  const [text, setText] = useState([]);
  const ws = useRef(null);

  useEffect(() => {
    ws.current = new WebSocket(process.env.REACT_APP_REST_API_WS + "/console");
    ws.current.onopen = () => setText(text => [...text, "Connecting to Server"]);
    console.log(text)
    ws.current.onclose = () => setText(text => [...text, "Connection Closed"]);

    return () => {
      ws.current.close();
    };
  }, []);

  useEffect(() => {
    if (!ws.current) return;

    ws.current.onmessage = e => {
      const message = JSON.parse(e.data);
      setText(text => [...text, message.m])
    };
  }, []);

  return (
    <div style={{padding: 20}}>
      <p>
        {
          text.map(m => {
            return <>{m}<br /></>
          })
        }
      </p>
    </div>
  );
}
export default Console