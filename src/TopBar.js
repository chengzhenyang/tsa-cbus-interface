import React from 'react';
import { Navbar, Nav } from 'react-bootstrap'
import Login from './Login';

function TopBar() {
    return (
        <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand href="#">TSA C-Bus Interface</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#/users">Users</Nav.Link>
                    <Nav.Link href="#/devices">Devices</Nav.Link>
                    <Nav.Link href="#/discovery">Discovery</Nav.Link>
                    <Nav.Link href="#/console">Console</Nav.Link>
                </Nav>
                <Login />
            </Navbar.Collapse>

        </Navbar>
    );
}

export default TopBar;
