import React, { useState, useEffect } from 'react'
import { Card, Button, Row } from "react-bootstrap"
import Axios from 'axios'

const Discovery = () => {
    const [devices, setDevices] = useState([])

    useEffect(() => {
       Refresh()
    }, [])

    const Refresh = () => {
        Axios.get(process.env.REACT_APP_REST_API_URL + "/discovery", { withCredentials: true }).then((res) => {
            setDevices(res.data)
            console.log(res.data)
        })
    }

    return (
        <div style={{ padding: 5 }}>
            <Card style={{ maxWidth: 1000, margin: "auto" }}>
                <Card.Body>
                    <Row>
                        <Card.Title style={{ padding: 15 }}>Discovery</Card.Title>
                        <Button onClick={Refresh} variant="success" style={{ margin: "auto", marginRight: 15 }}>Refresh</Button>
                    </Row>
                    <pre>
                        {JSON.stringify(devices, null, 2) }
                    </pre>
                </Card.Body>
            </Card >
        </div >
    )
}

export default Discovery