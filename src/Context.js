import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios'
import Axios from 'axios';
export const Context = createContext();

const Provider = (props) => {
  const [show, setShow] = useState(false)
  const [login, setLogin] = useState(false)
  const [rooms, setRooms] = useState(null)
  const [users, setUsers] = useState([])

  useEffect(() => {
    RefreshRoom()
  }, [])

  const RefreshRoom = () => {
    Axios.get(process.env.REACT_APP_REST_API_URL + "/room", { withCredentials: true }).then((res) => {
      setLogin(true)
      setRooms(res.data)
      console.log(res.data)
    }).catch(err => {
      alert("Not Logged In")
    })
  }
  useEffect(() => {
      Axios.get(process.env.REACT_APP_REST_API_URL + "/user", { withCredentials: true }).then((res) => {
          setUsers(res.data)
          console.log(res.data)
      })
  }, [])

  return (
    <Context.Provider value={{
      show, setShow,
      rooms, setRooms,
      login, setLogin,
      users, setUsers
    }}>
      {props.children}
    </Context.Provider>
  )
}

export default Provider