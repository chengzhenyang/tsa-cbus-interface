import React, { useState, useEffect, useContext } from 'react'
import { Card, Button, Table, Row } from "react-bootstrap"
import Axios from 'axios'
import { Context } from './Context'

const Users = () => {

    const {users} = useContext(Context)

    return (
        <div style={{ padding: 5 }}>
            <Card style={{ maxWidth: 1000, margin: "auto" }}>
                <Card.Body>
                    <Row>
                        <Card.Title style={{ padding: 15 }}>Users</Card.Title>
                        <Button variant="secondary" style={{ margin: "auto", marginRight: 15 }}>Add User</Button>
                    </Row>
                    <div style={{ overflowX: "scroll" }}>
                        <Table striped bordered hover size="sm" style={{ minWidth: 800 }}>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>RoomID</th>
                                    <th>Username</th>
                                    <th>Token</th>
                                    <th>IsAdmin</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    users === null ? null : users.map((user, key) => {
                                        return (
                                            <tr>
                                                <td>{user.id}</td>
                                                <td>{user.roomid}</td>
                                                <td>{user.username}</td>
                                                <td>{user.token}</td>
                                                <td>{user.isadmin? "true": "false"}</td>
                                            </tr>
                                        )

                                    })
                                }

                            </tbody>
                        </Table>
                    </div>
                </Card.Body>
            </Card >
        </div >
    )
}

export default Users